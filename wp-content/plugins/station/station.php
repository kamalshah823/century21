<?php
/*
Plugin Name: Station Taxonomy
Plugin URI: http://nepaldigital.com/
Description: A simple taxonomy activation plugin
Version: 1.0
Author: Global Digital
Author URI: http://nepaldigital.com
License: GPL
*/


add_action( 'init', 'register_station' );
function register_station() {

	$labels = array(
		"name" => "station",
		"label" => "Stations",
		"menu_name" => "Stations",
		"all_items" => "All Stations",
		"edit_item" => "Edit Station",
		"view_item" => "View Station",
		"update_item" => "Update Station Name",
		"add_new_item" => "Add New Station",
		"new_item_name" => "New Station Name",
		"parent_item" => "Parent Station",
		"parent_item_colon" => "Parent Station:",
		"search_items" => "Search Staions",
		"popular_items" => "Popular Stations",
		);

	$args = array(
		"labels" => $labels,
		"hierarchical" => true,
		"label" => "Stations",
		"show_ui" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'station', 'with_front' => true ),
		"show_admin_column" => false,
	);
	register_taxonomy( "station", array( "property" ), $args );

// End cptui_register_my_taxes
}

?>
