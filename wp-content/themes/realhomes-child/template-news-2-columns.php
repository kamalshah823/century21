<?php
/*
*   Template Name: News 2 Columns Template
*/
?>
<?php
get_header();
?>

    <!-- Page Head -->
    <?php get_template_part("banners/default_page_banner"); ?>

 <!-- Content -->

		<div class="container contents listing-grid-layout">
        <div class="row">
            <div class="span12 main-wrap">

                <!-- Main Content -->
                <div class="main">

                    <section class="listing-layout" style="float: left;">
						 <?php
                        $title_display = get_post_meta( $post->ID, 'REAL_HOMES_page_title_display', true );
                        if( $title_display != 'hide' ){
                            ?>
                           <!--  <h3 class="title-heading"><?php //the_title(); ?></h3> -->
                            <?php
                        }
                        ?>
		
                        <!-- news Container -->
                        <div id="gallery-container" style="padding: 30px 0;">
                            <div class="gallery-2-columns " style="height:auto !important; margin-left: 0px ;">
                                                         
											<?php
											 $postslist = get_posts('numberposts=10&order=DESC&orderby=date');
											 foreach ($postslist as $post) :
												setup_postdata($post);
											 ?>
											<div class="gallery-item isotope-item " style="margin-left:34px;">
											 <figure><?php the_post_thumbnail('gallery-two-column-image'); ?></figure>
											 <h5 class="item-title entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
											 <p><?php $content = get_the_content(); echo mb_strimwidth($content, 0, 400, '...');?></p>
											<a href="<?php the_permalink(); ?>" class="more-details">Read More <i class="fa fa-caret-right"></i></a>
											</div>
										
											 <?php endforeach; ?>
																		   
										
                               
                            </div>
                        </div>
                        <!-- end of gallery container -->
					</section>
				</div>	
			</div>

        </div><!-- End contents row -->

    </div><!-- End Content -->

<?php get_footer(); ?>